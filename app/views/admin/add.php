<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
  <meta charset="UTF-8">
  <title>Daftar Berita | PT Cendana Teknika Utama</title>
  <?php $this->load->view('template/head'); ?>
  <link rel="stylesheet" href="../assets/css/admin.css">
</head>
<body>
  <?php $this->load->view('template/header_admin'); ?>
  <section class="mainbar">
    <div class="content">
      <h1>Tambah Berita</h1>
      <form>
        <table>
          <tr>
              <td>
                Nama Berita
              </td>
              <td>
                <input type="text" placeholder="Nama Berita">
              </td>
            </tr>
            <tr>
              <td>
                Deskripsi Berita
              </td>
              <td>
                <textarea cols="70" rows="20"></textarea>
              </td>
            </tr>
            <tr>
              <td>
                Kategori
              </td>
              <td>
                <input type="text" placeholder="Kategori">
              </td>
            </tr>
            <tr>
              <td>
                Unggah Gambar
              </td>
              <td>
                <input type="file">
              </td>
            </tr>
            <tr>
              <td></td>
              <td>
                <input type="submit" value="Simpan" />
                <input type="reset" value="Reset" />
              </td>
            </tr>
        </table>
      </form>
    </div>
  </section>
</body>
</html>