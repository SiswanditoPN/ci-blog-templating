<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
  <meta charset="UTF-8">
  <title>Daftar Berita | PT Cendana Teknika Utama</title>
  <?php $this->load->view('template/head'); ?>
  <link rel="stylesheet" href="../assets/css/admin.css?1234">
</head>
<body>
  <?php $this->load->view('template/header_admin'); ?>
  <section class="mainbar">
    <div class="content">
      <h1>Daftar Berita</h1>
      <table>
        <tr>
          <th>Nomor</th>
          <th>Judul Berita</th>
          <th>Kategori</th>
          <th>Isi Berita</th>
          <th>Foto</th>
          <th>Aksi</th>
        </tr>
        <tr>
          <td>1</td>
          <td>
            Masa Pengenalan Lingkungan Sekolah (MPLS) 2018
          </td>
          <td>
            Kesiswaan
          </td>
          <td>
            &emsp;Masa Pengenalan Lingkungan Sekolah (MPLS) adalah salah satu agenda wajib yang dilaksanakan sekolah untuk memperkenalkan lingkungan sekolah kepada para peserta didik baru di sekolahnya. SMK Negeri 8 Malang melaksanakan kegiatan MPLS selama lima hari, mulai 16 Juli - 20 Juli 2017. Acara ini dibuka dengan apel pagi oleh seluruh warga SMK Negeri 8 Malang sekaligus penyematan kartu identitas kepada perwakilan peserta MPLS.
          </td>
          <td>
            <img src="../assets/images/foto/berita1.jpeg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
          </td>
          <td>
            <a href="#"><button>Sunting</button></a>
            <a href="#"><button>Hapus</button></a>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>
            Pembekalan Praktik Kerja Lapangan (PKL) Gelombang I 2018
          </td>
          <td>
            Humas
          </td>
          <td>
            &emsp;Sebelum melaksanakan praktik kerja lapangan (PKL) untuk peserta didik kelas XI B dan D, SMK Negeri 8 Malang melaksanakan pembekalan. Acara ini dilaksanakan pada hari Kamis (19/7) pukul 08.00 - hingga selesai di aula SMK Negeri 8 Malang. Pembekalan ini dipimpin oleh Tim Humas.
          </td>
          <td>
            <img src="../assets/images/foto/berita2.jpg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
          </td>
          <td>
            <a href="#"><button>Sunting</button></a>
            <a href="#"><button>Hapus</button></a>
          </td>
        </tr>
        <tr>
          <td>3</td>
          <td>
              Halal Bihalal Guru dan Karyawan
          </td>
          <td>
            Humas
          </td>
          <td>
            &emsp;Halal bihalal guru karyawan pada (14/07) di Hotel Grand Cakra Araya yang diikuti oleh seluruh guru, karyawan dan para tamu undang SMK Negeri 8 Malang. Kegiatan ini di buka dengan bacaan doa dan ayat suci al qur'an. Setelah itu dilanjutkan dengan sambutan dari Bapak Drs. Hari Mulyono, M.T. selaku Kepala SMK Negeri 8 Malang.
          </td>
          <td>
            <img src="../assets/images/foto/berita3.jpg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
          </td>
          <td>
            <a href="#"><button>Sunting</button></a>
            <a href="#"><button>Hapus</button></a>
          </td>
        </tr>
        <tr>
          <td>4</td>
          <td>
            Informasi, Ketentuan dan Jadwal Pelaksanaan PPDB Jawa Timur 2018
          </td>
          <td>
            Humas
          </td>
          <td>
            &emsp;Informasi, ketentuan dan jadwal pelaksanaan Penerimaan Peserta Didik Baru (PPDB) Jawa Timur 2018 dapat diunduh di <a href="http://bit.ly/Asta_PPDBJatim2018_01">http://bit.ly/Asta_PPDBJatim2018_01</a>
          </td>
          <td>
            <img src="../assets/images/foto/berita4.png" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
          </td>
          <td>
            <a href="#"><button>Sunting</button></a>
            <a href="#"><button>Hapus</button></a>
          </td>
        </tr>
        <tr>
          <td>5</td>
          <td>
            Pelepasan Siswa dan Siswi Asta Arkananta Angkatan XI
          </td>
          <td>
            Kesiswaan
          </td>
          <td>
            &emsp;Resmi sudah! peserta didik kelas XII SMK Negeri 8 Malang angkatan XI tahun pelajaran 2017/2018 menjadi alumni setelah dinyatakan 100% lulus dan telah mengikuti serangkaian acara pelepasan di Aula Hotel Kartika Graha Malang pada hari Senin (7/5). Seluruh peserta didik kelas XII, orang tua/wali murid, serta Bapak/Ibu guru mengikuti acara dengan khidmat. Acara ini dibuka dengan cucuk lampah berupa tarian kemudian diikuti oleh panji-panji SMK Negeri 8 Malang, pada barisan inti terdapat Kepala SMK Negeri 8 Malang, Bapak Drs. Hari Mulyono, M.T. beserta wakil kepala sekolah dan kepala kompetensi keahlian, kemudian diikuti oleh Bapak/Ibu wali kelas XII.
          </td>
          <td>
            <img src="../assets/images/foto/berita5.jpg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
          </td>
          <td>
            <a href="#"><button>Sunting</button></a>
            <a href="#"><button>Hapus</button></a>
          </td>
        </tr>
      </table>
    </div>
  </section>
</body>
</html>