<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
  <meta charset="UTF-8">
  <title>Masuk | PT Cendana Teknika Utama</title>
  <?php $this->load->view('template/head'); ?>
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/login.css?1234">
</head>
<body>
  <div class="form-box">
    <div class="form-logo">
      <a href="<?=site_url('berita')?>">
        <img src="<?= base_url(); ?>assets/images/logo/Cendana-2.png" alt="Logo PT Cendana Teknika Utama" title="L ogo PT Cendana Teknika Utama">
      </a>
    </div>
    <form action="<?=site_url('admin/list')?>" method="POST">
      <div class="input">
        <input type="text" name="username" placeholder="Input your username">
      </div>
      <div class="input">
        <input type="password" name="password" placeholder="Input your password">
      </div>
      <div class="input">
        <input type="submit" class="submit" name="submit" value="Log In">
      </div>
    </form>
    <div class="info">
      <a href="#">Forgot your password?</a>
    </div>
    <div class="footer">
      &copy; 2018 PT Cendana Teknika Utama. All Rights Reserved.
    </div>
  </div>
</body>
</html>