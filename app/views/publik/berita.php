<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
  <meta charset="UTF-8">
  <title>Berita | PT Cendana Teknika Utama</title>
  <?php $this->load->view('template/head'); ?>
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/publik.css">
</head>
<body>
  <?php $this->load->view('template/navbar'); ?>
  <div class="box">
    <div class="header">
      <span class="title">Masa Pengenalan Lingkungan Sekolah (MPLS) 2018</span>
    </div>
    <div class="body">
      <div class="row">
        <div class="detail-berita-foto">
          <img src="assets/images/foto/berita1.jpeg" alt="Gambar berita pertama" title="Gambar berita pertama" width="750px" height="750px" />
        </div>
        <div class="detail-berita-narasi">
          <p>
            &emsp;Masa Pengenalan Lingkungan Sekolah (MPLS) adalah salah satu agenda wajib yang dilaksanakan sekolah untuk memperkenalkan lingkungan sekolah kepada para peserta didik baru di sekolahnya. SMK Negeri 8 Malang melaksanakan kegiatan MPLS selama lima hari, mulai 16 Juli - 20 Juli 2017. Acara ini dibuka dengan apel pagi oleh seluruh warga SMK Negeri 8 Malang sekaligus penyematan kartu identitas kepada perwakilan peserta MPLS. Selama MPLS diisi dengan berbagai materi, seperti pengenalan lingkungan dan kebijakan sekolah, identitas sekolah hingga pendidikan lainnya. Pemateri berasal dari berbagai elemen warga sekolah, dari Kepala Sekolah, Kesiswaan, Kurikulum, Sapras, Humas, Kepala Paket Keahlian, TU dan Tim BK. Selain itu, Pemateri didatangkan dari luar sekolah, baik dari unsur TNI, Kepolisian, Pusat Kesehatan hingga Tim Profauna. Dikemas dengan tema "kekerasan bukan bagian dari kami" , dengan kegiatan MPLS diharapkan para peserta mampu mengenal almamaternya dengan baik untuk memudahkan mereka dalam mengikuti Kegiatan Belajar Mengajar di Asta Arkananta sekaligus untuk menghilangkan stigma buruk MOS yang terbentuk dengan lama. Sehinga, MPLS adalah sarana pengenalan sekolah, bukan untuk kekerasan. Setelah apel pagi semua warga Asta Arkananta melakukan halal bihalal untuk mengingat idul Fitri 1439.<br /><br />Reporter : Hani maulidyah<br />Editor : Dita Ismawanda<br />Fotografer : Devita Anggraeni, Ria Widia Prastika, dan Rizky Fajar S.
          </p>
        </div>
        <div class="berita-foto">
          
        </div>
      </div>
    </div>
    <?php $this->load->view('template/footer'); ?>
  </div>
</body>
</html>