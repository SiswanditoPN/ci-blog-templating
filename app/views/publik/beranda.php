<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
  <meta charset="UTF-8">
  <title>Beranda | PT Cendana Teknika Utama</title>
  <?php $this->load->view('template/head'); ?>
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/publik.css">
</head>
<body>
  <?php $this->load->view('template/navbar'); ?>
  <div class="box">
    <div class="header">
      <span class="title">Beranda</span>
    </div>
    <div class="body">
      <div class="row">
        <!-- Row 1 -->
        <div class="berita-judul">
          Masa Pengenalan Lingkungan Sekolah (MPLS) 2018
        </div>
        <div class="berita-narasi">
          <p>
            &emsp;Masa Pengenalan Lingkungan Sekolah (MPLS) adalah salah satu agenda wajib yang dilaksanakan sekolah untuk memperkenalkan lingkungan sekolah kepada para peserta didik baru di sekolahnya. SMK Negeri 8 Malang melaksanakan kegiatan MPLS selama lima hari, mulai 16 Juli - 20 Juli 2017. Acara ini dibuka dengan apel pagi oleh seluruh warga SMK Negeri 8 Malang sekaligus penyematan kartu identitas kepada perwakilan peserta MPLS. <a href="<?=site_url('berita/detail')?>">[Baca Selengkapnya...]</a>
          </p>
        </div>
        <div class="berita-foto">
          <img src="<?= base_url(); ?>assets/images/foto/berita1.jpeg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
        </div>
      </div>
      <div class="row">
        <!-- Row 2 -->
        <div class="berita-judul">
          Pembekalan Praktik Kerja Lapangan (PKL) Gelombang I 2018
        </div>
        <div class="berita-narasi">
          <p>
            &emsp;Sebelum melaksanakan praktik kerja lapangan (PKL) untuk peserta didik kelas XI B dan D, SMK Negeri 8 Malang melaksanakan pembekalan. Acara ini dilaksanakan pada hari Kamis (19/7) pukul 08.00 - hingga selesai di aula SMK Negeri 8 Malang. Pembekalan ini dipimpin oleh Tim Humas.<!-- Pemberian materi diberikan oleh PT Retgoo yaitu tentang aturan yang boleh dan tidak boleh dilakukan di dalam dunia kerja.Reporter : Reidemptio Axxel Devanda Nugraha Fotografer : Daniel Riky Okta Priantono Editor : Hani Maulidyah dan Siswandito Putro Nugroho--> <a href="#">[Baca Selengkapnya...]</a>
          </p>
        </div>
        <div class="berita-foto">
          <img src="<?= base_url(); ?>assets/images/foto/berita2.jpg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
        </div>
      </div>
      <div class="row">
        <!-- Row 3 -->
        <div class="berita-judul">
          Halal Bihalal Guru dan Karyawan
        </div>
        <div class="berita-narasi">
          <p>
           &emsp;Halal bihalal guru karyawan pada (14/07) di Hotel Grand Cakra Araya yang diikuti oleh seluruh guru, karyawan dan para tamu undang SMK Negeri 8 Malang. Kegiatan ini di buka dengan bacaan doa dan ayat suci al qur'an. Setelah itu dilanjutkan dengan sambutan dari Bapak Drs. Hari Mulyono, M.T. selaku Kepala SMK Negeri 8 Malang. <!--Kemudian diisi ceramah oleh dosen. Disela-sela acara Panitia juga memberikan hadiah untuk anak-anak guru dan karyawan yang sudah memegang nomor karcis berhadiah yang sudah dibagi oleh panitia. setelah itu, para guru karyawan dan para tamu undangan saling bersalam salam dan mengucapkan "minal aidzin walfaidzin, mohon maaf lahir dan batin" dan diakhiri dengan makan siang bersama. Tujuan dari acara ini guna untuk selalu menjalin silahturahmi para guru karyawan dan para tamu yang datang. . Reporter : Hana Widya Astuti Editor : Dita Ismawanda dan Risky Fajar Sulistyo Fotografer : Hana Widya Astutik dan Siswandito Putro Nugroho--> <a href="#">[Baca Selengkapnya...]</a>
          </p>
        </div>
        <div class="berita-foto">
          <img src="<?= base_url(); ?>assets/images/foto/berita3.jpg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
        </div>
      </div>
      <div class="row">
        <!-- Row 4 -->
        <div class="berita-judul">
          Informasi, Ketentuan dan Jadwal Pelaksanaan PPDB Jawa Timur 2018
        </div>
        <div class="berita-narasi">
          <p>
            &emsp;Informasi, ketentuan dan jadwal pelaksanaan Penerimaan Peserta Didik Baru (PPDB) Jawa Timur 2018 dapat diunduh di <a href="http://bit.ly/Asta_PPDBJatim2018_01" target="_blank">http://bit.ly/Asta_PPDBJatim2018_01</a> <!--Reporter : Siswandito Putro Nugroho Desainer : Muhammad Rifan Hidayat dan Siswandito Putro Nugroho--> <a href="#">[Baca Selengkapnya...]</a>
          </p>
        </div>
        <div class="berita-foto">
          <img src="<?= base_url(); ?>assets/images/foto/berita4.png" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
        </div>
      </div>
      <div class="row">
        <!-- Row 5 -->
        <div class="berita-judul">
          Pelepasan Siswa dan Siswi Asta Arkananta Angkatan XI
        </div>
        <div class="berita-narasi">
          <p>
            &emsp;Resmi sudah! peserta didik kelas XII SMK Negeri 8 Malang angkatan XI tahun pelajaran 2017/2018 menjadi alumni setelah dinyatakan 100% lulus dan telah mengikuti serangkaian acara pelepasan di Aula Hotel Kartika Graha Malang pada hari Senin (7/5). Seluruh peserta didik kelas XII, orang tua/wali murid, serta Bapak/Ibu guru mengikuti acara dengan khidmat. Acara ini dibuka dengan cucuk lampah berupa tarian kemudian diikuti oleh panji-panji SMK Negeri 8 Malang, pada barisan inti terdapat Kepala SMK Negeri 8 Malang, Bapak Drs. Hari Mulyono, M.T. beserta wakil kepala sekolah dan kepala kompetensi keahlian, kemudian diikuti oleh Bapak/Ibu wali kelas XII.<!-- Usai pembukaan, acara ini dilanjut dengan sambutan dari kepala SMK Negeri 8 Malang, Dinas Pendidikan propinsi Jawa Timur cabang Kota Malang dan Batu, komite SMK Negeri 8 Malang serta ucapan terima kasih sekaligus pamitan dari perwakilan peserta didik kelas XII. "Pelepasan ini bukan berarti benar-benar lepas namun kalian masih keluarga besar SMK Negeri 8 Malang sehingga kami sangat mengharapkan kritik dan saran dari alumni." ujar kepala SMK Negeri 8 Malang pada sambutannya. Setelah itu, tiba saatnya acara yang ditunggu-tunggu yaitu penghargaan peserta didik berprestasi akademik maupun non akademik yang dilanjut dengan pelepasan peserta didik kelas XII SMK Negeri 8 Malang per kelas yang diiringi oleh tim paduan suara. Acara ini diakhiri dengan pesan penutupan oleh kepala sekolah yang kemudian dilanjutkan dengan sesi foto bersama per kelas bersama kepala sekolah dan wali kelas. . Reporter : Ria Widiani Prastika Editor : Elsa Lailia Salsabillah, Hani Maulidyah, Rafenia Febriani Sabila dan Siswandito Putro Nugroho Fotografer : Rizky Fajar Sulistyo--> <a href="#">[Baca Selengkapnya...]</a>
          </p>
        </div>
        <div class="berita-foto">
          <img src="<?= base_url(); ?>assets/images/foto/berita5.jpg" alt="Gambar berita pertama" title="Gambar berita pertama" width="150px" height="150px" />
        </div>
      </div>
    </div>
    <?php $this->load->view('template/footer'); ?>
  </div>
</body>
</html>