<header>
  <div class="nav left">
    <ul>
      <li>
        <a href="<?=site_url('berita')?>">Beranda</a>
      </li>
      <li>
        <a href="<?=site_url('berita/detail')?>">Berita</a>
      </li>
    </ul>
  </div>
  <div class="nav mid">
    <a href="index.html">
      <img src="<?= base_url(); ?>assets/images/logo/Cendana-2.png" alt="Logo PT Cendana Teknika Utama" title="Logo PT Cendana Teknika Utama">
    </a>
  </div>
  <div class="nav right">
    <ul>
      <li>
        <a href="">Kontak</a>
      </li>
      <li>
        <a href="">Profil</a>
      </li>
    </ul>
  </div>
</header>