<div class="footer-wrapper">
  <div class="footer">
    <div class="footer-left">
      <p>
        <strong>MALANG ( KANTOR PUSAT )</strong><br />
        Jl. Soekarno Hatta Ruko Permata Griya Shanta NR. 24 – 25 Malang – Jawa Timur<br />
        Telp: 0341 – 496 497, 488<br />
        Fax: 0341 – 408 657<br />
        Email: layanan@cendana2000.com
      </p>
    </div>
    <div class="footer-right">
      <p>
        <strong>JAKARTA ( KANTOR CABANG )</strong><br />
        Jl. Kebagusan Raya no 192, Pasar Minggu, Jakarta Selatan 12550<br />
        Telp: 021-22978922
      </p>
    </div>
    <div class="footer-bottom">
      <span>&copy; 2018 PT Cendana Teknika Utama. All Rights Reserved.</span>
    </div>
  </div>
</div>