<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/index');
	}

	public function add()
	{
		$this->load->view('admin/add');
	}

	public function list()
	{
		$this->load->view('admin/list');
	}


}

/* End of file Admin.php */
/* Location: .//E/P32018/ci-blog-templating/app/controllers/Admin.php */
?>