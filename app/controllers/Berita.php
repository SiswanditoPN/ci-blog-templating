<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function index()
	{
		$this->load->view('publik/beranda');
	}

	public function detail()
	{
		$this->load->view('publik/berita');
	}

}

/* End of file Berita.php */
/* Location: .//E/P32018/ci-blog-templating/app/controllers/Berita.php */
?>